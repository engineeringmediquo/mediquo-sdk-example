package com.mediquo.example.sdk

import android.os.Bundle
import android.view.View
import com.mediquo.example.sdk.databinding.ActivitySplashBinding
import kotlinx.coroutines.*

class SplashActivity : BaseActivity() {

    private lateinit var job: Job
    private lateinit var binding: ActivitySplashBinding
    override val contentView: View
        get() = binding.content

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onResume() {
        super.onResume()
        process()
    }

    private fun process() {
        job = CoroutineScope(Dispatchers.Main).launch {
            delay(SPLASH_DELAY)
            if (preferences?.contains(CURRENT_CLIENT_CODE) == true) {
                navigateToMain()
            } else {
                navigateToLogin()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        job.cancel()
    }
}