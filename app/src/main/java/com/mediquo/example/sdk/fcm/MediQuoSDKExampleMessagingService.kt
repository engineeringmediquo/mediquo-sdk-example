package com.mediquo.example.sdk.fcm

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.mediquo.chat.MediquoSDK
import com.mediquo.example.sdk.FCM_TAG

class MediQuoSDKExampleMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        /* Delegate remoteMessage to mediQuo SDK */
        Log.d(FCM_TAG, "From: $remoteMessage")
        MediquoSDK.getInstance()?.onFirebaseMessageReceived(remoteMessage)
    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        /* Register push token to mediQuo SDK */
        Log.d(FCM_TAG, "Refreshed token: $newToken")
        MediquoSDK.getInstance()?.registerPushToken(newToken)
    }
}