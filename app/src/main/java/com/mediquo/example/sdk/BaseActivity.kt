package com.mediquo.example.sdk

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

abstract class BaseActivity: AppCompatActivity() {

    abstract val contentView: View
    protected var preferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContainer()
        preferences = (application as? App)?.getMyPreferences()
    }

    protected fun showSnackBar(message: String) {
        Snackbar.make(contentView, message, Snackbar.LENGTH_SHORT).show()
    }

    protected fun navigateToMain() {
        startActivity(Intent(this, MainActivity::class.java))
        this.finish()
    }

    protected fun navigateToLogin() {
        Intent(this, LoginActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        }.run { startActivity(this) }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setUpContainer() {
        // Any touch outside the EditTexts hides the keyboard
        window.decorView.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                val imm: InputMethodManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
            }
            false
        }
    }
}