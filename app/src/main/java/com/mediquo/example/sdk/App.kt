package com.mediquo.example.sdk

import android.app.Application
import android.content.Context
import android.content.IntentFilter
import android.content.SharedPreferences
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.mediquo.chat.MediquoSDK

class App : Application() {

    private lateinit var sharedPref: SharedPreferences
    private val mediquoSDKReceiver = SDKReceiver()

    fun getMyPreferences(): SharedPreferences? {
        return if (this::sharedPref.isInitialized) {
            sharedPref
        } else {
            null
        }
    }

    override fun onCreate() {
        super.onCreate()
        sharedPref = getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE)
        sharedPref.getString(CURRENT_API_KEY, null)?.let { key ->
            MediquoSDK.initialize(this, key)
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mediquoSDKReceiver,
            IntentFilter(getString(R.string.mediquo_local_broadcast_event))
        )
    }

}