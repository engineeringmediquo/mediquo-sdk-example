package com.mediquo.example.sdk

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.mediquo.chat.presentation.events.*

class SDKReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {

        val eventType = intent.extras?.getString(EVENT_KEY)
        Log.d(SDK_EVENT_TAG, "Received $eventType")

        val message = intent.extras?.getString(MESSAGE)
        val hash = intent.extras?.getString(PRO_HASH)
        val specialityId = intent.extras?.getInt(SPECIALITY_ID, -1)

        when (eventType) {
            CHAT_VIEW -> { /* Event chat view */ }
            CHAT_MESSAGE_RECEIVED -> { /* Event message received */ }
            CHAT_MESSAGE_SENT -> { /* Event message sent */ }
            PROFESSIONAL_PROFILE_VIEW -> { /* Event professional profile view */ }
            MEDICAL_HISTORY_VIEW -> { /* Event medical history view */ }
            ALLERGIES_VIEW -> { /* Event allergies view */ }
            ILLNESSES_VIEW -> { /* Event illnesses view */ }
            MEDICATIONS_VIEW -> { /* Event medications view */ }
            REPORTS_VIEW -> { /* Event reports view */ }
            RECIPES_VIEW -> { /* Event recipes view */ }
            CALL_STARTED -> { /* Event call started */ }
            CALL_ENDED -> { /* Event call ended */ }
            VIDEOCALL_STARTED -> { /* Event videocall started */ }
            VIDEOCALL_ENDED -> { /* Event videocall ended */ }
        }
    }
}