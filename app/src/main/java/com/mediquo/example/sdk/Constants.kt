package com.mediquo.example.sdk

const val SPLASH_DELAY = 2000L

const val FCM_TAG = "FCM"
const val INIT_TAG = "Initialize"
const val AUTH_TAG = "Authenticate"
const val DE_AUTH_TAG = "DeAuthentication"

const val SDK_EVENT_TAG = "SDKEvent"

const val LIST_TAG = "ProfessionalList"

const val PREFERENCES_KEY = "sdk_example_preferences"
const val CURRENT_API_KEY = "current_api_key"
const val CURRENT_CLIENT_CODE = "current_client_code"