package com.mediquo.example.sdk

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import com.mediquo.chat.MediquoDeAuthenticateListener
import com.mediquo.chat.MediquoSDK
import com.mediquo.chat.MediquoUnreadMessagesListener
import com.mediquo.chat.presentation.features.professionals.ProfessionalListFragment
import com.mediquo.chat.presentation.features.professionals.ProfessionalListListener
import com.mediquo.example.sdk.databinding.ActivityMainBinding

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    override val contentView: View
        get() = binding.content

    private val professionalList: ProfessionalListFragment by lazy { ProfessionalListFragment() }

    private val deAuthenticateListener = object : MediquoDeAuthenticateListener {
        override fun onSuccess() {
            Log.d(DE_AUTH_TAG, "success")
            logout()
        }

        override fun onFailure(message: String?) {
            Log.d(DE_AUTH_TAG, "failure $message")
        }
    }

    private val listListener = object : ProfessionalListListener {
        override fun onListLoaded() {
            /* List of professionals loaded */
            Log.d(LIST_TAG, "List loaded")
        }

        override fun onProfessionalClick(
            professionalId: Long,
            specialityId: String?,
            hasAccess: Boolean
        ): Boolean {
            /* Return boolean value based on your business rules to open or not a chat */
            Log.d(
                LIST_TAG,
                "clicked : professionalId $professionalId specialityId $specialityId hasAccess $hasAccess"
            )
            return true
        }

        override fun onUnreadMessageCountChange(unreadMessageCount: Int?) {
            /* Number of unread messages after load list */
            Log.d(LIST_TAG, "UnreadMessageCount Changed $unreadMessageCount")
        }

    }

    private val unreadMessagesLister = object : MediquoUnreadMessagesListener {
        override fun onResponse(count: Int) {
            showSnackBar(
                resources.getQuantityString(
                    R.plurals.unread_messages_message, count, count
                )
            )
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setFragment(professionalList)
        setProfessionalListListener(listListener)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.mediquo_medical_history_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if (MediquoSDK.getInstance()?.isAuthenticated == true) {
            menu?.findItem(R.id.login_logout)?.title = getString(R.string.logout)
        } else {
            menu?.findItem(R.id.login_logout)?.title = getString(R.string.login)
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_medical_history -> MediquoSDK.getInstance()?.openMedicalHistory(this)

            R.id.menu_allergies -> MediquoSDK.getInstance()?.openAllergiesActivity(this)

            R.id.menu_diseases -> MediquoSDK.getInstance()?.openDiseasesActivity(this)

            R.id.menu_medications -> MediquoSDK.getInstance()?.openMedicationsActivity(this)

            R.id.menu_reports -> MediquoSDK.getInstance()?.openReportsActivity(this)

            R.id.menu_prescriptions -> MediquoSDK.getInstance()?.openPrescriptionsActivity(this)

            R.id.menu_unread_messages -> fetchUnreadMessages()

            R.id.login_logout -> logInLogOut()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.content, fragment)
            .commitAllowingStateLoss()
    }

    private fun setProfessionalListListener(listener: ProfessionalListListener) {
        professionalList.setProfessionalListListener(listener)
    }

    private fun fetchUnreadMessages() {
        MediquoSDK.getInstance()?.getUnreadMessageCount(unreadMessagesLister)
    }

    private fun logInLogOut() {
        if (MediquoSDK.getInstance()?.isAuthenticated == true) {
            MediquoSDK.deAuthenticate(deAuthenticateListener)
        } else {
            logout()
        }
    }

    private fun logout() {
        preferences?.edit()?.remove(CURRENT_CLIENT_CODE)?.apply()
        preferences?.edit()?.remove(CURRENT_API_KEY)?.apply()
        navigateToLogin()
    }

}