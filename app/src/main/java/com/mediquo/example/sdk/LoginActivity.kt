package com.mediquo.example.sdk

import android.os.Bundle
import android.util.Log
import android.view.View
import com.mediquo.chat.MediquoAuthenticateListener
import com.mediquo.chat.MediquoInitListener
import com.mediquo.chat.MediquoSDK
import com.mediquo.example.sdk.databinding.ActivityLoginBinding

class LoginActivity : BaseActivity() {

    private lateinit var binding: ActivityLoginBinding
    override val contentView: View
        get() = binding.content

    private val mediQuoInitListener = object : MediquoInitListener {
        override fun onFailure(message: String?) {
            /* Your initialization has failed */
            Log.d(INIT_TAG, "failure $message")
            showSnackBar(if (message.isNullOrBlank()) getString(R.string.error_message) else message)
        }

        override fun onSuccess() {
            /* Your initialization has been successful */
            Log.d(INIT_TAG, "success")
            preferences?.edit()?.putString(CURRENT_API_KEY, binding.etApiKey.text.toString())?.apply()
            authenticateOnMediquo(binding.etCode.text.toString())
        }
    }

    private val authenticateListener = object : MediquoAuthenticateListener {
        override fun onSuccess() {
            Log.d(AUTH_TAG, "success")
            preferences?.edit()?.putString(CURRENT_CLIENT_CODE, binding.etCode.text.toString())?.apply()
            navigateToMain()
        }

        override fun onFailure(message: String?) {
            Log.d(AUTH_TAG, "failure $message")
            showSnackBar(if (message.isNullOrBlank()) getString(R.string.error_message) else message)
            preferences?.edit()?.remove(CURRENT_CLIENT_CODE)?.apply()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnLogin.setOnClickListener {
            initMediquoSDK(binding.etApiKey.text.toString())
        }
    }

    private fun initMediquoSDK(apiKey: String) {
        MediquoSDK.initialize(applicationContext, apiKey, mediQuoInitListener)
    }

    private fun authenticateOnMediquo(code: String) {
        MediquoSDK.authenticate(code, authenticateListener)
    }
}